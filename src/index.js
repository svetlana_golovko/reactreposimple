import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from "react-datepicker";
import { format } from "date-fns";
import { Text, StyleSheet } from "react-native";

import './index.css';
import "react-datepicker/dist/react-datepicker.css";

const styles = StyleSheet.create({
    container: {
        marginTop: 50,
    },
    bigBlue: {
        color: 'blue',
        fontWeight: 'bold',
    },
    red: {
        color: 'red',
    },
});

const options = [
    {label: "", value: "0",},
    {label: "Apple", value: "1",},
    {label: "Mango", value: "2",},
    {label: "Banana", value: "3",},
    {label: "Pineapple", value: "4",},
];

class NameInput extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.onNameChange(e.target.value);
    }

    render() {
        const name = this.props.clientName;
        return (
            <fieldset>
                <legend>What is your name:</legend>
                <input value={name}
                       onChange={this.handleChange}/>
            </fieldset>
        );
    }
}

class FruitInput extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.onFruitChange(e.target.value);
    }

    render() {
        const name = this.props.clientName;
        const nameInStyle = <Text style={styles.bigBlue}>{name}</Text>
        const selectedFruit = this.props.favouriteFruit;
        return (
            <fieldset>
                <legend>What is your {nameInStyle} favourite fruit:</legend>
                <select value={selectedFruit} onChange={this.handleChange}>
                    {options.map((option) => (
                        <option key={option.value} value={option.value}>{option.label}</option>
                    ))}
                </select>
            </fieldset>
        );
    }
}

class BirthInput extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value, e) {
        this.props.onBirthdayChange(value);
    }

    render() {
        const name = this.props.clientName;
        const nameInStyle = <Text style={styles.bigBlue}>{name}</Text>
        const birthday = this.props.birthDay;
        return (
            <fieldset>
                <legend> Select your {nameInStyle} birthday :</legend>
                <DatePicker selected={birthday}
                            onChange={(value, e) => this.handleChange(value, e)}
                            dateFormat="dd-MM-yyyy"/>
            </fieldset>
        );
    }
}

class Output extends React.Component {

    render() {
        const name = this.props.clientName;
        const selectedFruit = this.props.favouriteFruit;
        const birthDay = this.props.birthDay;
        const selectedFruitName = (selectedFruit === "0") ? " not selected" :
            options
                .filter((option) => option.value === selectedFruit)
                .map((option) => option.label);
        console.log("selectedFruitName = " + selectedFruit);
        return (
            <fieldset>
                <legend>Dear Client:</legend>
                Your name is <Text style={styles.bigBlue}>{name}</Text>
                <p>Your favourite fruit is <Text style={styles.bigBlue}>{selectedFruitName}</Text> </p>
                <p>Your birthday is on <Text style={styles.bigBlue}>{birthDay}</Text></p>
            </fieldset>
        );
    }
}

class MyForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleFruitChange = this.handleFruitChange.bind(this);
        this.state = {clientName: '', favoriteFruit: '', birthDay: new Date()};
    }

    handleNameChange(clientName) {
        this.setState({clientName: clientName});
    }

    handleFruitChange(favoriteFruit) {
        this.setState({favoriteFruit: favoriteFruit});
    }

    handleBirthdayChange(birthDay) {
        this.setState({birthDay: birthDay});
    }

    handleBirthdayChange = date => {
        console.log(date)
        this.setState({birthDay: date});
    };

    render() {
        const clientName = this.state.clientName;
        const favouriteFruit = this.state.favoriteFruit;
        const birthDay = this.state.birthDay
        const birthDay2 = format(birthDay, "dd-MM-yyyy");
        return (
            <div>
                <NameInput
                    clientName={clientName}
                    onNameChange={this.handleNameChange}/>
                <FruitInput
                    clientName={clientName}
                    favouriteFruit={favouriteFruit}
                    onFruitChange={this.handleFruitChange}
                />
                <BirthInput clientName={clientName}
                            birthDay={birthDay}
                            onBirthdayChange={this.handleBirthdayChange}/>
                <Output
                    clientName={clientName}
                    favouriteFruit={favouriteFruit}
                    birthDay={birthDay2}
                />
            </div>
        );
    }
}

ReactDOM.render(
    <MyForm/>,
    document.getElementById('root')
);